from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import devUser


class devices(models.Model):
	devUser = models.ForeignKey(User)
	devEUI = models.CharField(max_length=20)

class credentials(models.Model):
	ttnUser = models.ForeignKey(User)
    ttnAppEUI = models.CharField(max_length=30)
    ttnAccessKey = models.CharField(max_length=60)

class dampdata(models.Model):
	dd_devEUI = models.CharField(max_length=20)
	dd_time = models.DateTimeField()
	dd_temperature=models.FloatField()
	dd_humidity=models.FloatField()

# Create your models here.
